<?php

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use PHPUnit\Framework\TestCase;

require_once('vendor/autoload.php');

class CrowdinTest extends TestCase{

    public const HOST = 'http://localhost:4444/wd/hub';

    /**
     * @RemoteWebDriver
     */
    public static $driver;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        $chromeOptions = new ChromeOptions();
        $chromeOptions->addArguments(['start-maximized']);

        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability('enableVNC', true);
        $capabilities->setCapability(ChromeOptions::CAPABILITY_W3C, $chromeOptions);

        self::$driver = RemoteWebDriver::create(self::HOST, $capabilities);
        self::$driver->get('https://accounts.crowdin.com/login');
    }

    public function getRandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < 8; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    public function testLogin(){

        $email = self::$driver->findElement(WebDriverBy::name('email_or_login'));
        $password = self::$driver->findElement(WebDriverBy::name('password'));

        $email->sendKeys('rrrommmzzzesss@gmail.com');
        $password->sendKeys('Asusamda10')->submit();

        self::$driver->wait()->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id('remember-me'))
        );

        $userName = self::$driver->findElement(WebDriverBy::xpath('/html/body/div[2]/div/div/div[2]/strong'));

        $this->assertSame('RomanDolinovskyi', $userName->getText());

        self::$driver->findElement(WebDriverBy::id('remember-me'))->click();

        self::$driver->wait()->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//div[@id="profile-page"]/div/div/div[1]/div/div[1]/div/h3'))
        );

        $accountUserName = self::$driver->findElement(WebDriverBy::xpath('//*[@id="profile-page"]/div/div/div[1]/div/div[1]/div/h3'));

        $this->assertSame('RomanDolinovskyi', $accountUserName->getText());
    }

    /**
     * @depends testLogin
     */
    public function testCreateProjectSource(){

        $projectLink = self::$driver->findElement(WebDriverBy::id('projects-menu-item'));
        $projectLink->click();
        sleep(1);
        $createLink = self::$driver->findElement(WebDriverBy::linkText('Create Project'));
        self::$driver->wait()->until(
            WebDriverExpectedCondition::visibilityOf($createLink)
        );
        $createLink->click();

        self::$driver->wait()->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="wrap"]/div[3]/div/div[1]/h1'))
        );

        $titleConfirm = self::$driver->findElement(WebDriverBy::xpath('//*[@id="wrap"]/div[3]/div/div[1]/h1'));

        $this->assertSame('Create Crowdin Project', $titleConfirm->getText());

    }

    /**
     * @depends testCreateProjectSource
     */
    public function testCreateProjectForm(){
        $projectName = self::$driver->findElement(WebDriverBy::id('project_name'));
        $projectNameString = 'SomeRandomProject'.$this->getRandomString();
        $projectName->sendKeys($projectNameString);

        $publicProjectCheckbox = self::$driver->findElement(WebDriverBy::id('join-policy-open'));
        $publicProjectCheckbox->click();


        $selectSourceLanguage = self::$driver->findElement(WebDriverBy::xpath('//*[@id="project_source_language_chosen"]/a'));
        $selectSourceLanguage->click();
        $selectSourceLanguageSelect = self::$driver->findElement(WebDriverBy::xpath('//*[@id="project_source_language_chosen"]/a'));
        $selectSourceLanguageSelect->click();
        $selectSourceLanguageInput = self::$driver->findElement(WebDriverBy::xpath('//*[@id="project_source_language_chosen"]/div/div/input'));
        $selectSourceLanguageInput->sendKeys('Ukrainian');
        $selectSourceLanguageField = self::$driver->findElement(WebDriverBy::xpath('//*[@id="project_source_language_chosen"]/div/ul/li'));
        $selectSourceLanguageField->click();
        $selectSourceLanguagePlaceholder = self::$driver->findElement(WebDriverBy::xpath('//*[@id="project_source_language_chosen"]/a/span'));
        $this->assertSame('Ukrainian', $selectSourceLanguagePlaceholder->getText());


        $translatedLanguageInput = self::$driver->findElement(WebDriverBy::xpath('//*[@id="target_languages_filter"]/div/input'));
        $translatedLanguageInput->sendKeys('English');
        sleep(1);
        $englishLanguage = self::$driver->findElement(WebDriverBy::xpath('//*[@id="target_languages_list"]/div/ul/li[1]/label/input'));
        $englishLanguage->click();
        $translatedLanguageInput->clear();


        $translatedLanguageInput->sendKeys('Italian');
        sleep(1);
        $italianLanguage = self::$driver->findElement(WebDriverBy::xpath('//*[@id="target_languages_list"]/div/ul/li[1]/label/input'));
        $italianLanguage->click();



        $englishLanguageSelect = self::$driver->findElement(WebDriverBy::xpath('//*[@id="target_languages_result"]/ul/li[1]/div'));
        $this->assertSame('English', $englishLanguageSelect->getText());
        $italianLanguageSelect = self::$driver->findElement(WebDriverBy::xpath('//*[@id="target_languages_result"]/ul/li[2]/div'));
        $this->assertSame('Italian', $italianLanguageSelect->getText());

        $submitBtn = self::$driver->findElement(WebDriverBy::id('create_project'));
        $submitBtn->click();

        self::$driver->wait()->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="project-header"]/div/h1/span'))
        );

        $titleConfirm = self::$driver->findElement(WebDriverBy::xpath('//*[@id="project-header"]/div/h1/span'));

        $this->assertSame($projectNameString, $titleConfirm->getText());
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::$driver->quit();
    }
}